import pigpio
import pwmInterface as pwm

class timerGPIO:
    def __init__(
            self, 
            pi, 
            callback, 
            pwmPin=9, 
            timerPin=10,
            freq=200 
        ):

        self.pi = pi
        self.timerPin = timerPin
        self.callback = callback
        self.pwmFreq = freq 

        # init for PWM
        self.pwm = pwm.PwmInterface(self.pi,pwmPin=pwmPin,dirPin1=None,dirPin2=None,pwmFreq=self.pwmFreq)
        self.pwm.setDuty(.5)

        # init for timer pin
        self.pi.set_mode(timerPin, pigpio.INPUT)
        self.pi.set_pull_up_down(timerPin, pigpio.PUD_UP)

        self.cb = self.pi.callback(timerPin, pigpio.RISING_EDGE, self._pulse)

    def _pulse(self, gpio, level, tick):
        self.callback(self.pwmFreq)

    def cancel(self):
        self.cbA.cancel()
        self.cbB.cancel()
