import aubio
import numpy as np
import pyaudio
import time
import argparse
import queue
import music21

class pitchDetector:
    def __init__(self):
        self.input_device_index=2
        self.volume_thresh=0.001
        self.minDB = -30

        # PyAudio object.
        self.p = pyaudio.PyAudio()

        # Open stream.
        self.stream = self.p.open(format=pyaudio.paFloat32,
                        channels=1, rate=44100, input=True,
                        input_device_index=self.input_device_index, frames_per_buffer=4096)

        # Aubio's pitch detection.
        self.pDetection = aubio.pitch("default", 2048, 2048//2, 44100)
        # Set unit.
        self.pDetection.set_unit("Hz")
        self.pDetection.set_silence(self.minDB)
        self.q = queue.Queue()
        self.current_pitch = music21.pitch.Pitch()

    def continuousPrint(self, printOut=False):
        while True:
            data = self.stream.read(1024, exception_on_overflow=False)
            samples = np.fromstring(data,
                                    dtype=aubio.float_type)
            pitch = self.pDetection(samples)[0]

            # Compute the energy (volume) of the
            # current frame.
            volume = np.sum(samples**2)/len(samples) * 100

            if pitch and volume > self.volume_thresh:  # adjust with your mic!
                self.current_pitch.frequency = pitch
            else:
                continue

            if printOut:
                print(self.current_pitch.freq440)
            
            else:
                current = self.current_pitch.nameWithOctave
                q.put({'Note': current, 'Cents': self.current_pitch.microtone.cents})

    def getCurrentPitch(self):
        data = self.stream.read(1024, exception_on_overflow=False)
        samples = np.fromstring(data, dtype=aubio.float_type)
        pitch = self.pDetection(samples)[0]

        # Compute the energy (volume) of the current frame.
        volume = np.sum(samples**2)/len(samples) * 100

        if pitch and volume > self.volume_thresh:  # adjust with your mic!
            self.current_pitch.frequency = pitch
            return pitch

        else: 
            return 0 


pd = pitchDetector()
for i in range(10000):
    time.sleep(0.02)

    startTime = time.time()
    
    pitch = pd.getCurrentPitch() 
    if pitch < 90:
        continue

    elif (pitch - pd.getCurrentPitch())<4:
        if (pitch - pd.getCurrentPitch())<4:
            print(pitch)
            print(time.time()-startTime)


