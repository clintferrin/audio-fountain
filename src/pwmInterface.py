import pigpio 

class PwmInterface:
    def __init__(
            self, 
            pi,
            pwmFreq=200,
            pwmPin=4, 
            dirPin1=2, 
            dirPin2=3
        ):
        self.pi = pi 
        self.pwmPin = pwmPin 
        self.dirPin1 = dirPin1 
        self.dirPin2 = dirPin2 
        self.setFreq(pwmFreq)

    def setDuty(self,percent):
        percent = self.cyclePercent(percent)
        self.pi.set_PWM_dutycycle(self.pwmPin,percent) 

    def setDirection(self,direction):
        if direction is "forward":
            self.pi.write(self.dirPin1,0)
            self.pi.write(self.dirPin2,1)
        
        elif direction is "backward":
            self.pi.write(self.dirPin1,1)
            self.pi.write(self.dirPin2,0)
    
        else:
            self.pi.write(self.dirPin1,0)
            self.pi.write(self.dirPin2,0)
            print("Incorrect input!")
            print("Pass forwards or backwards")

    def getFreq(self):
        return self.pi.get_PWM_frequency(self.pwmPin)

    def setFreq(self,freq):
        self.pi.set_PWM_frequency(self.pwmPin,freq)

    def cyclePercent(self,percent):
        output = percent*255
        if output > 255:
            print("Max Clipping")
            output = 255
        elif output < 0:
            print("Min Clipping")
            output = 0
        return output 
