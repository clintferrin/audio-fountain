import pigpio 
import time
import rotaryEncoder as en
import pwmInterface as pwm 
import numpy as np
import matplotlib.pyplot as plt
import gpioTimer as tm

ticksToRev = 0.001214919 

pos = 0
posArr = np.array([0])
timeArr = np.array([0])
timeStart = 0

def main():
    global posArr
    global timeArr 
    global timeStart 
    timerFreq = 50 # Hz
    pi = pigpio.pi()

    encoder = en.Encoder(pi, callbackEncoderSilent, gpioA=8, gpioB=7)
    timer = tm.timerGPIO(pi,callbackTimer,freq=timerFreq)
    motor = pwm.PwmInterface(pi,pwmPin=4,dirPin1=2,dirPin2=3)

    print("Forward") 
    motor.setDirection("forward")
    timeStart = time.time()
    motor.setDuty(.5)
    time.sleep(1.5)

    print("Backward") 
    motor.setDirection("backward")
    motor.setDuty(.5)
    time.sleep(1.5)

    motor.setDuty(0)
    encoder.cancel
    print("Plotting motor route") 

    plt.plot(timeArr,posArr)
    plt.title("Encoder sampled at " + str(timerFreq) + "Hz")
    plt.xlabel("Time (sec)")
    plt.ylabel("Rev/sec")
    plt.savefig("../data/" + str(timerFreq) + "Hz")
    plt.show()

def callbackPrint(way):
    global pos
    pos += way
    print("pos={}".format(pos))

def callbackPlot(way):
    global pos
    global posArr
    global timeArr 
    global timeStart
    pos += way
    posArr = np.append(posArr,pos)
    timeArr = np.append(timeArr,time.time()-timeStart)

def callbackEncoderSilent(way):
    global pos
    pos += way

def callbackTimer(freq):
    global pos
    global posArr
    global timeArr 
    posTmp = pos
    pos = 0
    posArr = np.append(posArr,posTmp*freq*ticksToRev)
    timeArr = np.append(timeArr,timeArr[-1]+1/freq)
    

if __name__ == '__main__':
  main()
