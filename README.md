# Audio Responsive Fountain 

## Project Description 
The audio responsive fountain connects to a microphone, and will adjust the water height and armature location based on input amplitude and pitch respectively. 

## Prerequisites
`matplotlib`
`scipy`  
`sounddevice`  

## Getting Set-up

## Running

## Developing Diarization Methods

## Testing
Unit tests are a great way to help debug. Writing a unit test is easy with the nosetests framework. 

All you do is: 
1. Create a `TestSomething.py` (must start with 'test') in the tests folder. 
2. Add methods into it to test each function of your class or method. Test methods names should start with 'test'. 
3. Test methods should also make liberal use of `assert` statements. These are used to say 'If this statement is false, the test should fail'. eg `assert x == 4`
4. Then, in the terminal, `cd` to the tests folder and run `nosetests`. You might have to install it first, use pip or apt. nosetests with then go through all the tests within the tests folder and show you which ones pass and which ones fail.

## Authors
* Clint Ferrin
* Ian Parry
* Jill Howell
